using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

namespace RPG
{

[RequireComponent(typeof(Unit))]
public class AI : MonoBehaviour
{
  Unit self;
  Unit player;

  Vector3 origin_position;
  public float max_distance = 10.0f;
  public float safe_distance = 7.0f;
  public float friend_distance = 12.0f;

  public float run_time = 3.0f;

#if UNITY_EDITOR
  void OnDrawGizmosSelected()
  {
    UnityEditor.Handles.color = Color.green;
    UnityEditor.Handles.DrawWireDisc(transform.position , transform.up, max_distance);

    UnityEditor.Handles.color = Color.red;
    UnityEditor.Handles.DrawWireDisc(transform.position , transform.up, safe_distance);
  }
#endif

  void Start()
  {
    G.self.enemy_count++;

    origin_position = transform.position;
    self = GetComponent<Unit>();
    player = GameObject.FindGameObjectWithTag("Player").GetComponent<Unit>();
    transform.Rotate(transform.up, Random.Range(0.0f, 360.0f));

    var finded_ally = GameObject.FindGameObjectsWithTag("Enemy");
    foreach(var friend in finded_ally)
    {
      if(friend == gameObject)
        continue;

      if((friend.transform.position - transform.position).magnitude < friend_distance)
        ally.Add(friend.GetComponent<Unit>());
    }
  }

  public List<Unit> ally = new List<Unit>();

  bool NeedReturnToOrigin()
  {
    return (origin_position - transform.position).magnitude > max_distance;
  }

  bool PlayerInSafeDistance()
  {
    return (player.transform.position - transform.position).magnitude < safe_distance;
  }

  bool PlayerInTrouble()
  {
    return PlayerInSafeDistance() && !NeedReturnToOrigin();
  }

  void OnDestroy()
  {
    G.self.enemy_count--;
  }

  void Update()
  {
    if(player == null)
      return;

    if(NeedReturnToOrigin() && (Time.time - self.last_attack) > run_time)
      self.SetTarget(origin_position);

    if(self.target == null)
    {
      if(PlayerInTrouble())
        self.SetTarget(player);
      else
      {
        foreach(var ally in ally)
        {
          if(ally == null || ally.target == null)
            continue;

          self.SetTarget(ally.target);
          break;
        }
      }
    }
  }
}

}

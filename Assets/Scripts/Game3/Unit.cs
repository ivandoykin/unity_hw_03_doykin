﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace RPG
{

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class Unit : MonoBehaviour
{
  NavMeshAgent agent;
  Animator animator;

  public Unit target;
  public Vector3? point = null;

  public float hp;
  public float hp_max = 100.0f;
  public float attack_radius = 4.0f;

  public float attack = 5.0f;

  public float attack_per_level = 1.0f;

  public bool in_attack = false;

  public float attack_per_second = 2.0f;

  public float regen_hp_speed = 10.0f;
  public float regen_hp_time = 2.0f;

  public float last_attack;

  bool wait_attack_time;

  public int level = 1;
  public float xp = 0.0f;

  enum State
  {
    idle,
    walk,
    attack
  }

  float last_hit_time = 0.0f;

  State current_state = State.idle;

  IEnumerator attack_coroutine = null;

  void Start()
  {
    animator = GetComponent<Animator>();

    agent = GetComponent<NavMeshAgent>();

    hp = hp_max;
  }

  public void SetTarget(Unit target)
  {
    point = null;
    this.target = target;
  }

  public void SetTarget(Vector3 point)
  {
    this.point = point;
    target = null;
    StopAttack();
  }

  Vector3 GetTargetPosition()
  {
    if(target == null)
      return Vector3.zero;

    return (transform.position - target.transform.position).normalized * (attack_radius / 2.0f) + target.transform.position;
  }

  public void MoveToPoint()
  {
    if(target != null)
      return;

    if(point == null)
      return;

    SetDestination(point.Value);

    if((transform.position - point.Value).magnitude < 1.0f)
      point = null;
  }

  void StopAttack()
  {
    if(attack_coroutine != null)
      StopCoroutine(attack_coroutine);
    in_attack = false;
  }

  void SetDestination(Vector3 destination)
  {
    PlayAnimation(State.walk);
    agent.SetDestination(destination);
  }

  void MoveToTarget()
  {
    if(target == null || in_attack)
      return;

    if(!IsTargetInAttackRadius())
      SetDestination(GetTargetPosition());
    else
      agent.ResetPath();
  }

  bool IsTargetInAttackRadius()
  {
    return target != null && Mathf.Abs((target.transform.position - transform.position).magnitude) <= attack_radius;
  }

  void AttackTarget()
  {
    if(IsTargetInAttackRadius() && !in_attack)
    {
      attack_coroutine = Attack();
      StartCoroutine(attack_coroutine);
      last_attack = Time.time;
    }
  }

  IEnumerator LookToTarget()
  {
    Vector3 direction = (target.transform.position - transform.position).normalized;
    Quaternion lookRotation = Quaternion.LookRotation(direction);

    if(lookRotation != transform.rotation && IsTargetInAttackRadius())
      PlayAnimation(State.walk);
    else
      yield break;

    while(lookRotation != transform.rotation && IsTargetInAttackRadius())
    {
      direction = (target.transform.position - transform.position).normalized;
      lookRotation = Quaternion.LookRotation(direction);

      transform.rotation = Quaternion.RotateTowards(transform.rotation, lookRotation, Time.deltaTime * agent.angularSpeed);
      yield return null;
      if(!IsTargetInAttackRadius())
        break;
    }
  }

  void MakeHit()
  {
    if(IsTargetInAttackRadius())
      target.Hit(this);

    wait_attack_time = false;
  }

  IEnumerator Attack()
  {
    animator.SetFloat("speed_attack", attack_per_second);

    yield return LookToTarget();

    in_attack = true;

    wait_attack_time = IsTargetInAttackRadius();
    if(wait_attack_time)
      PlayAnimation(State.attack);

    while(wait_attack_time)
      yield return null;

    if(!IsTargetInAttackRadius())
      PlayAnimation(State.idle);

    in_attack = false;
  }

  void Hit(Unit attacker)
  {
    hp -= attacker.attack;

    last_hit_time = Time.time;

    if(target == null && point == null)
      SetTarget(attacker);

    if(hp <= 0)
    {
      Destroy(gameObject);
      attacker.AddXP(GetKillXP());
    }
  }

  void AddXP(float xp)
  {
    this.xp += xp;
    CheckLevelUP();
  }

  void CheckLevelUP()
  {
    if(xp >= GetXPForLevelUP())
      LevelUP();
  }

  public float GetXPForLevelUP()
  {
    return level * 80.0f;
  }

  void LevelUP()
  {
    level++;
    hp_max += Mathf.Floor(hp_max * 0.2f);
    hp = hp_max;
    attack += attack_per_level;
    xp = 0.0f;
  }

  float GetKillXP()
  {
    return hp_max * 0.8f + attack;
  }

  void PlayAnimation(State state)
  {
    if(current_state == state)
      return;

    animator.ResetTrigger(current_state.ToString());
    current_state = state;
    animator.SetTrigger(state.ToString());
  }

  void RegenHP()
  {
    if(last_hit_time + regen_hp_time > Time.time)
      return;

    hp += regen_hp_speed * Time.deltaTime;
    if(hp > hp_max)
      hp = hp_max;
  }

  void Update()
  {
    animator.SetFloat("speed_walk", agent.velocity.magnitude / agent.speed);

    MoveToPoint();
    MoveToTarget();
    AttackTarget();

    RegenHP();

    if(target == null && point == null)
    {
      PlayAnimation(State.idle);
      in_attack = false;
    }
  }
}

}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game2
{

public class UIAbilities : MonoBehaviour
{
  Unit unit;
  public UIAbility origin;

  public void Init(Unit unit, Action<Unit.Ability> on_select)
  {
    this.unit = unit;
    foreach(var ability in unit.abilities)
    {
      var ui_ability = UIAbility.Instantiate(origin, origin.transform.parent);
      ui_ability.Init(ability, on_select);
      ui_ability.unit = unit;
    }
    origin.gameObject.SetActive(false);
  }

  public void SetActive(bool state)
  {
    gameObject.SetActive(state);
  }

  void Update()
  {
    if(unit == null)
      SetActive(false);
  }
}

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game2
{

public class TargetMarker : MonoBehaviour
{
  public Unit unit;

  Renderer object_renderer;

  void Start()
  {
    object_renderer = GetComponentInChildren<Renderer>();
  }

  void ShowChild(bool show)
  {
    for(int i = 0; i < transform.childCount; ++i)
      transform.GetChild(i).gameObject.SetActive(show);
  }

  void LateUpdate()
  {
    bool is_unit_null = unit == null || unit.Equals(null) || !unit.IsAlive();

    ShowChild(!is_unit_null);

    if(is_unit_null)
      return;

    transform.position = unit.transform.position;
    transform.forward = unit.transform.forward;
  }

}

}

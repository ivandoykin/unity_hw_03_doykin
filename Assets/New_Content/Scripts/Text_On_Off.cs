﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Text_On_Off : MonoBehaviour
{
    Text text;
    bool state;
    float checker;

    void Start()
    {
        state = false;
        text = GetComponent<Text>();
        text.enabled = state;
        StartCoroutine(Starting());
    }

    IEnumerator Starting ()
    {
        while (!(Input.GetMouseButtonDown(0))) // пока не кликаем мышкой
        {
            checker += Time.deltaTime;
            if (checker >= 0.5f) //ждёи 0.5 сек
            {
                if (state) { state = false; text.enabled = state; } // выключаем текст, если он был включен
                else { state = true; text.enabled = state; } // включаем текст, если он выключен
                checker = 0f;
            }
            yield return null;
        }
        text.gameObject.SetActive(false); // выключаем текст при клике мышкой  
    }

}

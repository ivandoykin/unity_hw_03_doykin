﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Level1_Load : MonoBehaviour
{
    public void NextScene()
    {
        SceneManager.LoadScene("Game1");
    }
}

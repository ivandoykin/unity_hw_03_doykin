﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Click_Catcher : MonoBehaviour
{
    Animator anima;

    void Start()
    {
        anima = GetComponentInParent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            anima.SetBool("is_starting", true);
        }
    }
}

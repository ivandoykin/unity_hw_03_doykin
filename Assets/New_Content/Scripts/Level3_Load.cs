﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Level3_Load : MonoBehaviour
{
    public void NextScene()
    {
        SceneManager.LoadScene("Game3");
    }
}